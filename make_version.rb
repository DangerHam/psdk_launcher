require_relative 'src/IODigester'
require_relative 'src/Helper'
require 'json'
require 'digest'

# Create all the game folder (in order to copy properly)
helper = Helper.new
helper.load_config
helper.make_game_folders
next_version = helper.game_version.sub(/([0-9]+)$/) { Regexp.last_match(1).to_i.next.to_s }

print "Enter next version [#{next_version}]: "
input_version = gets.chomp
next_version = input_version unless input_version.empty?

print 'Enter Release path: '
release_path = gets.chomp.gsub('"', '').gsub('\\', '/')

# Test release path
all_necessary_folder_exists = %w[audio Data pokemonsdk graphics].all? { |folder| Dir.exist?(File.join(release_path, folder))}
unless all_necessary_folder_exists
  puts 'One of the necessary folder does not exist, please provide a valid release path or compile your project properly'
  exit
end

# Copy all audio files & store them in file_list
file_list = []
(
  Dir[File.join(release_path, 'audio', '**/*')] +
  Dir[File.join(release_path, 'graphics', '**/*')] +
  Dir[File.join(release_path, 'Fonts', '**/*')]
).each do |filename|
  next if File.directory?(filename)

  sub_name = filename.sub(release_path, '')
  file_list << sub_name
  target_name = File.join(helper.game_path, sub_name)
  if !File.exist?(target_name) || File.size(filename) != File.size(target_name)
    puts "Copying #{sub_name}..."
    IO.copy_stream(filename, target_name)
  else
    puts "Skipping #{sub_name}..."
  end
end

# Make all the SHA1 we need
puts "Reading files to digest"
sha1_list = Dir[File.join(release_path, 'Data', '*')] +
            Dir[File.join(release_path, 'pokemonsdk', '**/*')]
sha1_list.reject! { |filename| File.directory?(filename) }
sha1_list << File.join(release_path, 'Game.yarb')
sha1_file_contents = sha1_list.map do |filename|
  Thread.new do
    Thread.current[:data] = [filename.sub(release_path, ''), File.binread(filename)]
  end
end
sha1_file_contents.map!(&:join)
sha1_file_contents.map! { |thr| thr[:data] }
sha1_file_contents << ['config.json', { game_version: next_version }.to_json]
puts "Digesting files"
sha1_hashes = sha1_file_contents.map { |(k, v)| [k, Digest::SHA1.hexdigest(v)] }.to_h

# Copy all the sha1
sha1_file_contents.each do |(filename, data)|
  complete_filename = File.join(helper.game_path, filename)
  puts "Copying #{complete_filename}..."
  File.binwrite(complete_filename, data)
end

Dir.mkdir('versions') unless Dir.exist?('versions')
version_data_json = { files: file_list, hashes: sha1_hashes }.to_json
Dir['versions/*.json'].each do |filename|
  File.binwrite(filename, version_data_json)
end
File.binwrite("versions/#{helper.game_version}.json", version_data_json)
