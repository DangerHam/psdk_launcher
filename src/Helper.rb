class Helper
  KNOWN_FOLDERS = %w[
    pokemonsdk pokemonsdk/master Saves graphics graphics/shaders Fonts Data
    audio audio/bgm audio/bgs audio/me audio/particles audio/se
    audio/se/cries audio/se/mining_game audio/se/moves audio/se/voltorbflip
    plugins
  ]
  # Get the launcher config
  def load_config
    @config = {
      game_url: 'https://download.psdk.pokemonworkshop.com/downloads/',
      version: File.read('pokemonsdk/version.txt').to_i
    }
    @config[:version_string] = [@config[:version]].pack('I>').unpack('C*').join('.').gsub(/^(0\.)+/, '')
  end

  # Test if the connexion with the server works
  # @return [Binding] a binding with the variables connected as boolean/string and thread as Thread
  def test_connection
    connected = false
    thread = Thread.new do
      uri = URI(@config[:game_url])
      Net::HTTP.start(uri.host, uri.port, open_timeout: 2.0, use_ssl: uri.port != 80) do |http|
        http.request(Net::HTTP::Get.new(uri)) do |resp|
          connected = resp.code.to_i == 200 ? true : 'Error: unrecognized server'
        end
      end
    rescue StandardError => e
      connected = "Error: Cannot connect"
    end
    return binding
  end

  # Test if a new version exists
  # @return [Binding] a binding with the variable new_version as boolean, a thread as Thread and version as string (all files info)
  def test_new_version
    new_version = false
    version = nil
    thread = Thread.new do
      uri = URI(File.join(@config[:game_url], @config[:version].to_s, 'file_index.txt'))
      Net::HTTP.start(uri.host, uri.port, open_timeout: 2.0, use_ssl: uri.port != 80) do |http|
        http.request(Net::HTTP::Get.new(uri)) do |resp|
          next unless resp.code.to_i == 200

          new_version = true
          version = @config[:version].to_s
        end
      end
    rescue StandardError => e
      new_version = false
    end
    return binding
  end

  # Create the game folder and known folders
  def make_game_folders
    KNOWN_FOLDERS.each do |dir|
      mkdir(dir)
    end
  end

  # Get the game version text
  # @return [String]
  def game_version_text
    return @config[:version_string]
  end

  # Get the game version
  # @return [Integer]
  def game_version
    return @config[:version]
  end

  # Get the game url
  # @return [String]
  def game_url
    @config[:game_url]
  end

  # Test if the game can be played
  # @return [Boolean]
  def can_be_played?
    true
  end

  private

  # Safely create a directory
  # @param path [String]
  def mkdir(path)
    File.delete(path) if File.file?(path)
    Dir.mkdir(path) unless Dir.exist?(path)
  end
end
