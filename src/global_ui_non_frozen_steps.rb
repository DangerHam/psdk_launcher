class GlobalUI
  private

  def perform_non_frozen_step
    case state
    when :download
      perform_download_step
    when :start
      perform_start_step
    else
      self.state = :frozen
    end
  end

  def perform_download_step
    last_version = @helper.game_version - 1
    while last_version < @helper.game_version
      last_version = @helper.game_version
      return unless (dler = create_downloader([File.join(last_version.to_s, 'file_index.txt')]))
      return unless download_files(dler)

      @helper = Helper.new
      @helper.load_config
      @version_text.text = @helper.game_version_text
    end
    self.state = @helper.can_be_played? ? :play_allowed : :frozen
  end

  # Create the downloader
  # @param files_to_download [Array<String>]
  # @return [Downloader, nil]
  def create_downloader(files_to_download)
    if files_to_download.empty?
      self.state = @helper.can_be_played? ? :play_allowed : :frozen
      return nil
    end

    @download_bar.state = :download
    return Downloader.new(files_to_download, @helper.game_url, @helper.game_version.to_s)
  end

  # Get the files to download
  # @param dler [Downloader]
  # @return [Boolean]
  def download_files(dler)
    dler.start do |progress|
      @download_bar.progress = progress.to_f
      @window.update
    end
    if dler.failed?
      @error_text.text = dler.error
      self.state = :frozen
      return false
    end
    return true
  end

  def perform_start_step
    system('psdk.bat debug')
    self.state = :play_allowed
  end
end
