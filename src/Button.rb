class Button < SpriteStack
  LEGAL_STATES = %i[base hovered selected disabled]

  # Get the state of the button
  # @return [Symbol] :base, :hovered, :selected, :disabled
  attr_reader :state

  # Set/Get the onclick block to call
  # @return [Proc]
  attr_accessor :on_click

  # Create a new button
  # @param viewport [LiteRGSS::Viewport]
  # @param text [String]
  # @param state [Symbol] :base, :hovered, :selected, :disabled
  def initialize(viewport, text, state)
    super(viewport)
    @background = add_sprite(0, 0, 'button')
    @text = add_text(@background.width / 2, 4, 0, 18, text, 1)
    self.state = state
  end

  # Set the state of the button
  # @param state [Symbol] :base, :hovered, :selected, :disabled
  def state=(state)
    @state = state
    height = @background.bitmap.height / 4
    @background.src_rect.set(0, (LEGAL_STATES.index(state) || 0) * height, @background.width, height)
    @text.load_color(state == :disabled ? 1 : 2)
  end

  # Test if the mouse is in the button
  # @param x [Integer]
  # @param y [Integer]
  # @return [Boolean]
  def mouse_in?(x, y)
    return false unless visible

    x.between?(@background.x, @background.x + @background.width - 1) &&
      y.between?(@background.y, @background.y + @background.height - 1)
  end

  # Update a button depending on the mouse position and the reasons
  # @param x [Integer]
  # @param y [Integer]
  # @param reason [Symbol] :press or :release
  def update(x, y, reason)
    case reason
    when :press
      updade_press(mouse_in?(x, y))
    when :release
      update_release(mouse_in?(x, y))
    end
  end

  private

  # Perform the button state update when mouse is released
  # @param mouse_in [Boolean] if the mouse is in the button
  def updade_press(mouse_in)
    return if state == :disabled

    self.state = mouse_in ? :selected : :base
  end

  # Perform the button state update when mouse is pressed
  # @param mouse_in [Boolean] if the mouse is in the button
  def update_release(mouse_in)
    return if state == :disabled

    self.state = mouse_in ? :hovered : :base
  end
end
