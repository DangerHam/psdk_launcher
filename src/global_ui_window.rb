class GlobalUI
  private

  def create_window
    @window = LiteRGSS::DisplayWindow.new('Launcher', 800, 600, 1, 32, 0, true, false, true)
    @window.icon = LiteRGSS::Image.new(TEXTURES['pw_logo'], true)
  end

  def create_hooks
    @pressing = false
    @mouse_x = -256
    @mouse_y = -256
    @window.on_mouse_button_pressed = proc do |button|
      on_mouse_button_press(button)
    end
    @window.on_mouse_button_released = proc do |button|
      on_mouse_button_release(button)
    end
    @window.on_mouse_moved = proc do |x, y|
      on_mouse_moved(x, y)
    end
  end

  def on_mouse_button_press(button)
    return if button != Sf::Mouse::LEFT

    @pressing = true
    @start_button.update(@mouse_x, @mouse_y, :press)
    @download_button.update(@mouse_x, @mouse_y, :press)
    @start3_button.update(@mouse_x, @mouse_y, :press)
    @start6_button.update(@mouse_x, @mouse_y, :press)
    @startff_button.update(@mouse_x, @mouse_y, :press)
    @system_tag_button.update(@mouse_x, @mouse_y, :press)
    @town_map_button.update(@mouse_x, @mouse_y, :press)
    @upd_data_button.update(@mouse_x, @mouse_y, :press)
  end

  def on_mouse_button_release(button)
    return if button != Sf::Mouse::LEFT

    @pressing = false
    @start_button.update(@mouse_x, @mouse_y, :release)
    @start_button.on_click&.call if @start_button.mouse_in?(@mouse_x, @mouse_y)
    @download_button.update(@mouse_x, @mouse_y, :release)
    @download_button.on_click&.call if @download_button.mouse_in?(@mouse_x, @mouse_y)
    @start3_button.update(@mouse_x, @mouse_y, :release)
    @start3_button.on_click&.call if @start3_button.mouse_in?(@mouse_x, @mouse_y)
    @start6_button.update(@mouse_x, @mouse_y, :release)
    @start6_button.on_click&.call if @start6_button.mouse_in?(@mouse_x, @mouse_y)
    @startff_button.update(@mouse_x, @mouse_y, :release)
    @startff_button.on_click&.call if @startff_button.mouse_in?(@mouse_x, @mouse_y)
    @system_tag_button.update(@mouse_x, @mouse_y, :release)
    @system_tag_button.on_click&.call if @system_tag_button.mouse_in?(@mouse_x, @mouse_y)
    @town_map_button.update(@mouse_x, @mouse_y, :release)
    @town_map_button.on_click&.call if @town_map_button.mouse_in?(@mouse_x, @mouse_y)
    @upd_data_button.update(@mouse_x, @mouse_y, :release)
    @upd_data_button.on_click&.call if @upd_data_button.mouse_in?(@mouse_x, @mouse_y)
  end

  def on_mouse_moved(x, y)
    @mouse_x = x
    @mouse_y = y
    @start_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @download_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @start3_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @start6_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @startff_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @system_tag_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @town_map_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
    @upd_data_button.update(@mouse_x, @mouse_y, @pressing ? :press : :release)
  end
end
