require_relative 'IODigester'
require 'digest'
require 'stringio'

str_mb = 100
iteration_times = 40

str = ' ' * (1024 * 1024 * str_mb)
t = Time.new
iteration_times.times.map do
  Thread.new { Digest::SHA1.hexdigest(str) }
end.each(&:join)
p Time.new - t

t = Time.new
ios = iteration_times.times.map { |i| ["file_#{i}", StringIO.new(str)] }.to_h
digester = IODigester.new(Digest::SHA1, ios)
digester.start { |d| print format("\r%<progress>.2f%%", progress: d.progress * 100) }
puts("\n#{Time.new - t}         ")
p digester.hashes
p digester.failures
p Digest::SHA1.hexdigest(str)
