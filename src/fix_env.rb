# This script aim to fix environment variables to make sure the game gets an environment as clean as possible
def cleanup_env
  system_root = ENV['SystemRoot']
  to_remove = ENV.keys - %w[
    ALLUSERSPROFILE APPDATA CommonProgramFiles CommonProgramFiles(x86) COMPUTERNAME
    HOME NUMBER_OF_PROCESSORS OS PROCESSOR_ARCHITECTURE PROCESSOR_IDENTIFIER PROCESSOR_LEVEL PROCESSOR_REVISION
    ProgramData ProgramFiles ProgramFiles(x86) PUBLIC TMP TEMP SystemRoot windir USERDOMAIN
    USER USERDOMAIN USERPROFILE SSL_CERT_FILE __GL_THREADED_OPTIMIZATIONS
  ]
  to_remove.each { |k| ENV.delete(k) }
  ENV['PATHEXT'] = '.EXE;.RB'
  ENV['PATH'] = "#{system_root}\\system32;#{system_root};"
  ENV['RUBYOPT'] = '-Eutf-8'
  ENV['GAMEDEPS'] = File.expand_path('.')
  ENV['__GL_THREADED_OPTIMIZATIONS'] = '0'
end
cleanup_env
undef cleanup_env
