File.binwrite(
  'launcher',
  RubyVM::InstructionSequence.compile(
    %w[
      fix_env
      load_dependencies
      all_textures
      Fonts
      SpriteStack
      Button
      DonwloadBar
      Downloader
      Helper
      global_ui
      global_ui_window
      global_ui_states
      global_ui_non_frozen_steps
      global_ui_start
    ].map { |f| File.read("src/#{f}.rb") }.join("\n\n"),
    'launcher', '.'
  ).to_binary
)
File.write('start.rb', 'RubyVM::InstructionSequence.load_from_binary(File.binread(\'launcher\')).eval')
