require './lib/LiteRGSS'

resources = Dir['assets/*.png'].map do |filename|
  [File.basename(filename.sub('.png', '')).dump, LiteRGSS::Image.new(filename).to_png.dump].join(' => ')
end

File.binwrite('src/all_textures.rb', "TEXTURES = {\n#{resources.join(",\n")}\n}")
